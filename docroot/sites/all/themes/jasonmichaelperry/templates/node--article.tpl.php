<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>">

  <a href="<?php print $node_url; ?>"><?php print render($content['field_images']); ?></a>

  <?php if (!$page): ?>
    <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
  <?php endif; ?>

  <?php if ($page): ?>
    <h1<?php print $title_attributes; ?>><?php print $title; ?></h1>
  <?php endif; ?>

  <?php print render($content['field_subtitle']); ?>

  <?php
    // We hide the comments and links now so that we can render them later.
    hide($content['comments']);
    hide($content['links']);
    hide($content['field_subtitle']);
    hide($content['field_images']);
    print render($content);
  ?>

  <?php if ($page): ?>
    <?php print render($content['comments']); ?>
  <?php endif; ?>

</div> <!-- /node-->
